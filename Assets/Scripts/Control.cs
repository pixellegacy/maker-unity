using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour {
	//This file controls all of the references to every day classes.
	//After you've made a refence to this class, use control.nameOfScriptYouWantToAccess.methodInClass();
	public GizmoControl gizmo;
	public BlockFactory blockfactory;
	public Config config;
	public MakerGUI gui;
	public InputInterface input;
	public BlockHistory history;
	
	//fps shit
	public float updateInterval;
    private float lastInterval;
    private int frames;
	private int lifetimeFrames;
    public float fps;
	public string formattedFPS;
	public bool showFPS;
	
	void Start () {
		gizmo = GameObject.Find("Control").GetComponent<GizmoControl>();
		blockfactory = GameObject.Find("Control").GetComponent<BlockFactory>();
		config = GameObject.Find("Control").GetComponent<Config>();
		gui = GameObject.Find("Control").GetComponent<MakerGUI>();
		input = GameObject.Find("Control").GetComponent<InputInterface>();
		history = GameObject.Find("Control").GetComponent<BlockHistory>();
		
		//fps shit
		updateInterval = .5f;
		showFPS = true;
		lastInterval = Time.realtimeSinceStartup;
		Time.timeScale = 1f;
	}
 
	void Update()
	{
		frames++;
        float timeNow = Time.realtimeSinceStartup;
        if (timeNow > lastInterval + updateInterval) {
            fps = frames / (timeNow - lastInterval);
			formattedFPS = System.String.Format("FPS: {0:0}",fps);
            frames = 0;
            lastInterval = timeNow;
        }
	}
}
