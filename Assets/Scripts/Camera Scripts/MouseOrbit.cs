using UnityEngine;
using System.Collections;

public class MouseOrbit : MonoBehaviour {

	public Transform lockto;
	public float distance = 10.0f;
	public float height = 5.0f;

	public float xSpeed = 250.0f;
	public float ySpeed = 120.0f;

	public float yMinLimit = -20f;
	public float yMaxLimit = 80f;
	private float x = 0.0f;
	private float y = 0.0f;

	void Start () {
		Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;
		// Make the rigid body not change rotation
		if (rigidbody)
			rigidbody.freezeRotation = true;
	}
	
void LateUpdate () {
	if(Input.GetMouseButton(1)){
		Screen.lockCursor = true;
	}
	else{
		Screen.lockCursor = false;
	}
    if (lockto && Screen.lockCursor) {
        x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
        y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
 		
 		y = ClampAngle(y, yMinLimit, yMaxLimit);
 		       
        Quaternion rot = Quaternion.Euler(y, x, 0);
        Vector3 pos = rot * new Vector3(0.0f, 0.0f, -distance) + new Vector3(lockto.position.x, lockto.position.y + height, lockto.position.z);
        
        transform.rotation = rot;
        transform.position = pos;
    }
}

static float ClampAngle (float angle, float min, float max) {
	if (angle < -360)
		angle += 360;
	if (angle > 360)
		angle -= 360;
	return Mathf.Clamp (angle, min, max);
}
}
