using UnityEngine;
using System.Collections;

public class SkyControl : MonoBehaviour {
	public Color[] skyColors = {new Color(255,0,0), new Color(0,255,0), new Color(0,0,255)};
	public int NumberOfStripes = 16;
	private Texture2D tex;
	public GameObject sky;
	
	// Use this for initialization
	void Start () {
		changeSky(skyColors, NumberOfStripes);
		DontDestroyOnLoad(sky);
	}
	
	public void changeSky(Color[] cols, int NumStripes){
		if(tex){
			tex = (Texture2D)sky.renderer.material.GetTexture("_DownTex");
			tex.Resize(1,NumStripes);
			tex.Apply();
		}
		else{
			tex =  new Texture2D(1, NumStripes, TextureFormat.ARGB32, false);
		}
		
		int pos;
		for(var i = 0; i < NumStripes; i++ ) {
			pos = Mathf.FloorToInt(((float)i/(float)NumStripes)*((float)cols.Length-1f));
			Color currentColor = cols[pos];
			Color nextColor = cols[pos+1];
			float strength = (((float)i/(float)NumStripes)*((float)cols.Length-1f)) - pos;
			tex.SetPixel(0,i,(currentColor * (1-strength)) + (nextColor * strength));
		}
		tex.Apply(false);
		tex.filterMode = FilterMode.Point;
		sky.renderer.material.SetTexture("_DownTex", tex);
	}
}