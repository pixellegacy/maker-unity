using UnityEngine;
using System.Collections;

public class CameraMoveScript : MonoBehaviour {
	public float width, height, xoffset, yoffset;

	void Start () {
		camera.pixelRect = new Rect(0+xoffset,Screen.height-height-yoffset,width,height);
	}
}
