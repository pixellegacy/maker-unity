using UnityEngine;
using System.Collections;
using System;

//Beware, this file will scar your mind. It is the entire GUI for the program.
public class MakerGUI : MonoBehaviour {
	private Vector2 notificationScroll, materialScrollPosition;
	private string usernameField, passwordField, emailField;
	private Control cont;
	private bool autoLogin, doNotificationScrollBottom;
	private ArrayList notifications, notificationsTime, notificationsScreen;	private int currentNumTextures, lastMatTextureInt;
	private System.Object[] materialTextures;
	public Color DiffuseColor, EmissiveColor;	public String pixelTexture, resourcePrefix;	public Texture pixelTex;	public GameObject tpixelthing, pixel2thing;
	public GUISkin OnScreenText, Windows, EscapeMenu;
	public float screenWidth, screenHeight;	
	public Texture EscapeMenuBackground;
	public bool showOpenFileDialog, showConfirmOpenDialog, showSaveFileDialog, showOptionsDialog, showPopupDialog, showThumbnail, showLoginRegister;
	public Rect openFileDialog, confirmOpenDialog, saveFileDialog, optionsDialog, popupDialog, loginRegisterDialog;
	public Rect openFileButton, pixelButtonsArea, blockInformationText, materialsTabRect;
	public string blockURL, popupString, tempPosSnap, tempRotSnap, tempScaSnap;
	public int optionsPage, numWindowsOpen, loginRegisterInt, blockTypeHolder, transformTextFieldInt, materialsTexInt;
	public GUIContent[] optionsTabs;
	public Texture2D[] blockTypeTextures;
	public string[] tempSelectedPos, tempSelectedWorldPos, tempSelectedRot, tempSelectedWorldRot, tempSelectedSca, tempSelectedWorldSca;	
	void Start () {
		cont = GameObject.Find("Control").GetComponent<Control>();
		openFileDialog = new Rect(Screen.width/2-150, Screen.height/2-100, 300, 200);
		//optionsDialog = new Rect(Screen.width/2-250, Screen.height/2-100, 500, 200);
		popupDialog = new Rect(Screen.width/2-150, Screen.height/2-100, 300, 200);
		loginRegisterDialog = new Rect(Screen.width/2-150, Screen.height/2-100, 300, 200);
		openFileButton = new Rect(5, Screen.height-30, 100, 30);
		pixelButtonsArea = new Rect(5, 5, 50, 105);
		optionsTabs = new GUIContent[] {new GUIContent("Block","General block configuration"), new GUIContent("General","General shit"), new GUIContent("Gizmo","Hi i'm a gizmo"), new GUIContent("Keys","Configure keys here"),new GUIContent("Account","Your account for saving/loading"), new GUIContent("Log","Various messages are stored here")};
		optionsPage = 0;
		numWindowsOpen = 0;
		blockURL = "http://www.pixellegacy.com/unity/stuff/staticshrine.png"; //Default block URL
		usernameField = "";
		passwordField = "";
		emailField = "";
		notifications = new ArrayList();
		notificationsTime = new ArrayList();
		notificationsScreen = new ArrayList();
		blockInformationText = new Rect(80, 0, 200, 114);		materialsTabRect = new Rect(Screen.width-200, 0, 200, 400);
		tempSelectedPos = new string[3];
		tempSelectedWorldPos = new string[3];
		tempSelectedRot = new string[3];
		tempSelectedWorldRot = new string[3];
		tempSelectedSca = new string[3];
		notificationScroll = Vector2.zero;		DiffuseColor = new Color(1f,1f,1f,1f);		EmissiveColor = new Color(0f,0f,0f,1f);
		lastMatTextureInt = -1;
		resourcePrefix = "N8/";
	}		void Update(){		StartCoroutine(updatePixelColors());
		if((lastMatTextureInt != materialsTexInt) && cont.blockfactory.loadedTextures){
			lastMatTextureInt = materialsTexInt;
			switch (materialsTexInt){
				case 0: currentNumTextures = cont.blockfactory.n8Textures.Length;
						materialTextures = cont.blockfactory.n8Textures;
						resourcePrefix = "N8/";
						break;
				case 1: currentNumTextures = cont.blockfactory.particleTextures.Length;
						materialTextures = cont.blockfactory.particleTextures;
						resourcePrefix = "particles/";
						break;
				case 2: currentNumTextures = cont.blockfactory.symbolTextures.Length;
						materialTextures = cont.blockfactory.symbolTextures;
						resourcePrefix = "Symbols/";
						break;
				case 3: currentNumTextures = cont.blockfactory.tronicTextures.Length;
						materialTextures = cont.blockfactory.tronicTextures;
						resourcePrefix = "tronics/";
						break;
			}
		}	}
	
	void OnGUI(){		
		if(showOptionsDialog){ //If we are looking at the ESC menu
			GUI.skin = EscapeMenu;						//draw the background
			GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),EscapeMenuBackground,ScaleMode.StretchToFill);						//the navigation buttons
			optionsPage = GUI.SelectionGrid(new Rect(0,5,75,optionsTabs.Length*(EscapeMenu.customStyles[0].fixedHeight+EscapeMenu.customStyles[0].margin.top)), optionsPage, optionsTabs,1,EscapeMenu.customStyles[0]);
						//Save config button			if(GUI.Button(new Rect(0,Screen.height-35,75,30), new GUIContent("Save","Saves your config file."), EscapeMenu.customStyles[0])){
				try{
					showOptionsDialog = false;
					subWindow();
					StartCoroutine(cont.config.saveConfig());
				}
				catch{
					notification("Failed to save config file because the text fields were improperly formatted.");
				}
			}						//config rest button
			if(GUI.Button(new Rect(0,Screen.height-70,75,30), new GUIContent("Reset","Completely resets your config file."), EscapeMenu.customStyles[0])){
				cont.config.resetConfig();
			}
						//make a group to contain all of the actual screen contents for the esc menu
			GUI.BeginGroup(new Rect(80,10,Screen.width-85,Screen.height-5));
			if(optionsPage == 0){
				GUI.Label(new Rect(0,0,100,20),"Block name:");
				cont.blockfactory.blockName = GUI.TextField(new Rect(95,0,300,20),cont.blockfactory.blockName);
								//block type selectors -- could've probably been done better
				GUI.Label(new Rect(0,25,100,20),"Type:");
				GUI.BeginGroup(new Rect(0,40,400,40));
				if(cont.blockfactory.blockType == "Stuff"){ EscapeMenu.customStyles[14].normal.background = 						blockTypeTextures[0]; GUI.Button(new Rect(0,0,40,40), new GUIContent("","Stuff"), EscapeMenu.customStyles[14]);}
				else if(GUI.Button(new Rect(0,0,40,40), new GUIContent("","Stuff"), EscapeMenu.customStyles[4])){ 			cont.blockfactory.updateBlockType(cont.blockfactory.convertBlockType("Stuff"));}
				if(cont.blockfactory.blockType == "Hat"){ EscapeMenu.customStyles[14].normal.background = 						blockTypeTextures[1]; GUI.Button(new Rect(40,0,40,40), new GUIContent("","Hat"), EscapeMenu.customStyles[14]);}
				else if(GUI.Button(new Rect(40,0,40,40), new GUIContent("","Hat"), EscapeMenu.customStyles[5])){ 				cont.blockfactory.updateBlockType(cont.blockfactory.convertBlockType("Hat"));}
				if(cont.blockfactory.blockType == "Sword"){ EscapeMenu.customStyles[14].normal.background = 					blockTypeTextures[2]; GUI.Button(new Rect(80,0,40,40), new GUIContent("","Sword"), EscapeMenu.customStyles[14]);}
				else if(GUI.Button(new Rect(80,0,40,40), new GUIContent("","Sword"), EscapeMenu.customStyles[6])){ 			cont.blockfactory.updateBlockType(cont.blockfactory.convertBlockType("Sword"));}
				if(cont.blockfactory.blockType == "Useable"){ EscapeMenu.customStyles[14].normal.background = 					blockTypeTextures[3]; GUI.Button(new Rect(120,0,40,40), new GUIContent("","Useable"), EscapeMenu.customStyles[14]);}
				else if(GUI.Button(new Rect(120,0,40,40), new GUIContent("","Useable"), EscapeMenu.customStyles[7])){ 		cont.blockfactory.updateBlockType(cont.blockfactory.convertBlockType("Useable"));}
				if(cont.blockfactory.blockType == "Gun"){ EscapeMenu.customStyles[14].normal.background = 						blockTypeTextures[4]; GUI.Button(new Rect(160,0,40,40), new GUIContent("","Gun"), EscapeMenu.customStyles[14]);}
				else if(GUI.Button(new Rect(160,0,40,40), new GUIContent("","Gun"), EscapeMenu.customStyles[8])){ 			cont.blockfactory.updateBlockType(cont.blockfactory.convertBlockType("Gun"));}
				if(cont.blockfactory.blockType == "Shield"){ EscapeMenu.customStyles[14].normal.background = 					blockTypeTextures[5]; GUI.Button(new Rect(200,0,40,40), new GUIContent("","Shield"), EscapeMenu.customStyles[14]);}
				else if(GUI.Button(new Rect(200,0,40,40), new GUIContent("","Shield"), EscapeMenu.customStyles[9])){ 		cont.blockfactory.updateBlockType(cont.blockfactory.convertBlockType("Shield"));}
				if(cont.blockfactory.blockType == "Chest"){ EscapeMenu.customStyles[14].normal.background = 					blockTypeTextures[6]; GUI.Button(new Rect(240,0,40,40), new GUIContent("","Chest"), EscapeMenu.customStyles[14]);}
				else if(GUI.Button(new Rect(240,0,40,40), new GUIContent("","Chest"), EscapeMenu.customStyles[10])){ 		cont.blockfactory.updateBlockType(cont.blockfactory.convertBlockType("Chest"));}
				if(cont.blockfactory.blockType == "Pants"){ EscapeMenu.customStyles[14].normal.background = 					blockTypeTextures[7]; GUI.Button(new Rect(280,0,40,40), new GUIContent("","Pants"), EscapeMenu.customStyles[14]);}
				else if(GUI.Button(new Rect(280,0,40,40), new GUIContent("","Pants"), EscapeMenu.customStyles[11])){ 		cont.blockfactory.updateBlockType(cont.blockfactory.convertBlockType("Pants"));}
				if(cont.blockfactory.blockType == "Hands"){ EscapeMenu.customStyles[14].normal.background = 					blockTypeTextures[8]; GUI.Button(new Rect(320,0,40,40), new GUIContent("","Hands"), EscapeMenu.customStyles[14]);}
				else if(GUI.Button(new Rect(320,0,40,40), new GUIContent("","Hands"), EscapeMenu.customStyles[12])){		cont.blockfactory.updateBlockType(cont.blockfactory.convertBlockType("Hands"));}
				GUI.EndGroup();
								//debug saving thumbnail
				if(showThumbnail && cont.blockfactory.thumbnail){
					GUILayout.BeginArea(new Rect(Screen.width-32,Screen.height-64,32,32));
					GUILayout.Label(cont.blockfactory.thumbnail);
					GUILayout.EndArea();
				}
			}
			else if(optionsPage == 1){ //general				//gizmo movement speeds buttons
				GUILayout.BeginHorizontal();
				if(GUILayout.Button("",EscapeMenu.customStyles[2])) 
					if(cont.config.movementSpeed > 1f) cont.config.movementSpeed-=1f;
					else cont.config.movementSpeed = 1f;
				if(GUILayout.Button("",EscapeMenu.customStyles[1]))
					if(cont.config.movementSpeed < 100f) cont.config.movementSpeed+=1f;
					else cont.config.movementSpeed = 100f;
				
				GUILayout.Label("Movement speed: " + cont.config.movementSpeed.ToString("#"));
				GUILayout.EndHorizontal();
			}
			else if(optionsPage == 2){// gizmo				//gizmo snap settings
				GUILayout.BeginVertical();
				GUILayout.Label("Speed",EscapeMenu.customStyles[13]);
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("",EscapeMenu.customStyles[2])) if(cont.config.rotationSpeed > 1f) cont.config.rotationSpeed-=1f;else cont.config.rotationSpeed = 1f;
					if(GUILayout.Button("",EscapeMenu.customStyles[1])) if(cont.config.rotationSpeed < 100f) cont.config.rotationSpeed+=1f;else cont.config.rotationSpeed = 100f;
					GUILayout.Label("Rotation: " + cont.config.rotationSpeed.ToString("0"));
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("",EscapeMenu.customStyles[2])) if(cont.config.dragSpeed > 1f) cont.config.dragSpeed-=1f; else cont.config.dragSpeed = 1f;
					if(GUILayout.Button("",EscapeMenu.customStyles[1])) if(cont.config.dragSpeed < 100f) cont.config.dragSpeed+=1f; else cont.config.dragSpeed = 100f;
					GUILayout.Label("Drag: " + cont.config.dragSpeed.ToString("0"));
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("",EscapeMenu.customStyles[2])) if(cont.config.scaleSpeed > 1f) cont.config.scaleSpeed-=1f; else cont.config.scaleSpeed = 1f;
					if(GUILayout.Button("",EscapeMenu.customStyles[1])) if(cont.config.scaleSpeed < 100f) cont.config.scaleSpeed+=1f; else cont.config.scaleSpeed = 100f;
					GUILayout.Label("Scale: " + cont.config.scaleSpeed.ToString("0"));
				GUILayout.EndHorizontal();
				GUILayout.Label("Snap",EscapeMenu.customStyles[13]);
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("",EscapeMenu.customStyles[2])) if(cont.config.rotationSnap > 0f) cont.config.rotationSnap-=5f;else cont.config.rotationSnap = 0f;
					if(GUILayout.Button("",EscapeMenu.customStyles[1])) if(cont.config.rotationSnap < 180f) cont.config.rotationSnap+=5f;else cont.config.rotationSnap = 180f;
					GUILayout.Label("Rotation: " + cont.config.rotationSnap.ToString("0"));
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("",EscapeMenu.customStyles[2])) if(cont.config.positionSnap > 0f) cont.config.positionSnap-=.1f; else cont.config.positionSnap = 0f;
					if(GUILayout.Button("",EscapeMenu.customStyles[1])) if(cont.config.positionSnap< 50f) cont.config.positionSnap+=.1f; else cont.config.positionSnap = 50f;
					GUILayout.Label("Position: " + cont.config.positionSnap.ToString("0.###"));
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("",EscapeMenu.customStyles[2])) if(cont.config.scaleSnap > 0f) cont.config.scaleSnap-=.1f; else cont.config.scaleSnap = 0f;
					if(GUILayout.Button("",EscapeMenu.customStyles[1])) if(cont.config.scaleSnap < 50f) cont.config.scaleSnap+=.1f; else cont.config.scaleSnap = 50f;
					GUILayout.Label("Scale: " + cont.config.scaleSnap.ToString("0.###"));
				GUILayout.EndHorizontal();
				GUILayout.EndVertical();
				
			}
			else if(optionsPage == 3){//key config
				
			}
			else if(optionsPage == 4){ //account
				if(cont.config.userHash == ""){ //Not logged in
					loginRegisterInt = GUI.Toolbar(new Rect(0,30,200,20), loginRegisterInt, new GUIContent[]{new GUIContent("Login","If you have an account already."),new GUIContent("Register","If you need an account.")});
					if(loginRegisterInt == 0){ //login page
						GUI.Label(new Rect(0,0,200,40), "Log In", EscapeMenu.customStyles[13]);
						GUI.Label(new Rect(5,60,100,20), "Username: ");
						GUI.Label(new Rect(5,85,100,20), "Password: ");
						usernameField = GUI.TextField(new Rect(90,60,250,20), usernameField,32);
						passwordField = GUI.PasswordField(new Rect(90,85,250,20), passwordField, '*',32);
						autoLogin = GUI.Toggle(new Rect(110, 110, 120, 20), autoLogin, "Remember me");
						if(GUI.Button(new Rect(5,110, 100,20),"Log In")) cont.config.manualLogin(usernameField,passwordField, autoLogin ? 1 : 0);
					}
					else if(loginRegisterInt == 1){ //register page
						GUI.Label(new Rect(0,0,200,40), "Register", EscapeMenu.customStyles[13]);
						GUI.Label(new Rect(5,60,100,20), "Username: ");
						GUI.Label(new Rect(5,85,100,20), "Password: ");
						GUI.Label(new Rect(5,110,100,20), "Email: ");
						usernameField = GUI.TextField(new Rect(90,60,250,20), usernameField,32);
						passwordField = GUI.PasswordField(new Rect(90,85,250,20), passwordField, '*',32);
						emailField = GUI.TextField(new Rect(90,110,250,20), emailField);
						if(GUI.Button(new Rect(5,135, 100,20),"Register")) cont.config.manualRegister(usernameField,passwordField,emailField);
					}
				}
				else{ //logged in
					GUI.Label(new Rect(0,0,400,40), "Account Details", EscapeMenu.customStyles[13]);
					//GUI.Label(new Rect(0,40,300,20), "Hash: " + cont.config.userHash); //Don't want to tell them this as it's irrelevant and could be a security exploit in a way, not like they couldn't get it anyways
										if(GUI.Button(new Rect(5,60, 100,20),"Log Out")){ 
						cont.config.userHash = "";
						notification("Logged out.");
						usernameField = "";
						passwordField = "";
						emailField = "";
					}
				}
			}
			else if(optionsPage == 5){//notifications
				GUI.Label(new Rect(0,0,300,30),"Notifications", EscapeMenu.customStyles[13]);
				if(notifications != null){
					float t2 = Screen.height-50;
					float t = Mathf.Max(notifications.Count*20,t2);
					notificationScroll = GUI.BeginScrollView(new Rect(0, 30, Screen.width-90, t2), notificationScroll, new Rect(0, 0, Screen.width-125, t));
					for(int i = 0; i < notifications.Count; i++){
						GUI.Label(new Rect(0,0 + (i*20),EscapeMenu.GetStyle("Label").CalcSize(new GUIContent((string)notifications[i])).x,20), (string)notifications[i]);
					}
					if(doNotificationScrollBottom && notificationScroll[1] >= t - (t2) - 20) notificationScroll[1] = t;
					doNotificationScrollBottom = false;
					GUI.EndScrollView();
				}
			}
			GUI.EndGroup();
		}
		
		//Now, if we have the esc menu closed, basically normal GUI stuff
		else{
			GUI.skin = Windows;
			if(GUI.Button(openFileButton = new Rect(5, Screen.height-30, 100, 30), "Open File")){
				if(showOpenFileDialog){ 
					showOpenFileDialog = false;
					subWindow();
				}
				else{
					showOpenFileDialog = true;
					addWindow();
				}
			}					/*****************************			Pixel precision textboxes			******************************/
			if(cont.gizmo.SelectedTransform){ //If we currently have a block selected, show the precision edit menu, or at least it's tab.
				GUI.skin = OnScreenText;
				GUI.BeginGroup(blockInformationText);
				if(cont.config.togglePrecisionMenu){ //If the precision edit window is visible					blockInformationText = new Rect(80, 0, 200, 114);
					if(transformTextFieldInt == 0){ //If we are currently using the local coordinates
						if(GUI.Button(new Rect(174,22,20,20), "X", OnScreenText.customStyles[9])){ //If we pressed the "reset position" button
							cont.gizmo.SelectedTransform.parent.localPosition = Vector3.zero;
							updateTransformTextfields(1);
							cont.history.addHistory();
						}
						GUI.BeginGroup(new Rect(0,20,175,24), OnScreenText.customStyles[6]); //PositionTextboxGroup
						GUI.Label (new Rect(4,6,10,20),"P");
						GUI.SetNextControlName("SelectedPosX");
						tempSelectedPos[0] = GUI.TextField (new Rect(17,2,50,20), tempSelectedPos[0], OnScreenText.customStyles[3]);
						GUI.SetNextControlName("SelectedPosY");
						tempSelectedPos[1] = GUI.TextField (new Rect(70,2,50,20), tempSelectedPos[1], OnScreenText.customStyles[4]);
						GUI.SetNextControlName("SelectedPosZ");
						tempSelectedPos[2] = GUI.TextField (new Rect(123,2,50,20), tempSelectedPos[2], OnScreenText.customStyles[5]);
						GUI.EndGroup();
						if(GUI.Button(new Rect(174,47,20,20), "X", OnScreenText.customStyles[9])){//If we pressed the "reset rotation" button
							cont.gizmo.SelectedTransform.parent.localRotation = Quaternion.identity;
							updateTransformTextfields(2);
							cont.history.addHistory();
						}
						GUI.BeginGroup(new Rect(0,45,175,24),  OnScreenText.customStyles[6]); //RotationTextboxGroup
						GUI.Label (new Rect(4,6,10,20),"R");
						GUI.SetNextControlName("SelectedRotX");
						tempSelectedRot[0] = GUI.TextField (new Rect(17,2,50,20), tempSelectedRot[0], OnScreenText.customStyles[3]);
						GUI.SetNextControlName("SelectedRotY");
						tempSelectedRot[1] = GUI.TextField (new Rect(70,2,50,20), tempSelectedRot[1], OnScreenText.customStyles[4]);
						GUI.SetNextControlName("SelectedRotZ");
						tempSelectedRot[2] = GUI.TextField (new Rect(123,2,50,20), tempSelectedRot[2], OnScreenText.customStyles[5]);
						GUI.EndGroup();
					}
					else if(transformTextFieldInt == 1){ //If we are currently using the world coordinates
						if(GUI.Button(new Rect(174,22,20,20), "X", OnScreenText.customStyles[9])){//If we pressed the "reset position" button
							cont.gizmo.SelectedTransform.parent.position = Vector3.zero;
							updateTransformTextfields(1);
							cont.history.addHistory();
						}
						GUI.BeginGroup(new Rect(0,20,175,24), OnScreenText.customStyles[6]); //PositionTextboxGroup
						GUI.Label (new Rect(4,6,10,24),"P");
						GUI.SetNextControlName("SelectedWorldPosX");
						tempSelectedWorldPos[0] = GUI.TextField (new Rect(17,2,50,20), tempSelectedWorldPos[0], OnScreenText.customStyles[3]);
						GUI.SetNextControlName("SelectedWorldPosY");
						tempSelectedWorldPos[1] = GUI.TextField (new Rect(70,2,50,20), tempSelectedWorldPos[1], OnScreenText.customStyles[4]);
						GUI.SetNextControlName("SelectedWorldPosZ");
						tempSelectedWorldPos[2] = GUI.TextField (new Rect(123,2,50,20), tempSelectedWorldPos[2], OnScreenText.customStyles[5]);
						GUI.EndGroup();
						if(GUI.Button(new Rect(174,47,20,20), "X", OnScreenText.customStyles[9])){ //If we pressed the "reset rotation" button
							cont.gizmo.SelectedTransform.parent.rotation = Quaternion.identity;
							updateTransformTextfields(2);
							cont.history.addHistory();
						}
						GUI.BeginGroup(new Rect(0,45,175,24), OnScreenText.customStyles[6]); //RotationTextboxGroup
						GUI.Label (new Rect(4,6,10,24),"R");
						GUI.SetNextControlName("SelectedWorldRotX");
						tempSelectedWorldRot[0] = GUI.TextField (new Rect(17,2,50,20), tempSelectedWorldRot[0], OnScreenText.customStyles[3]);
						GUI.SetNextControlName("SelectedWorldRotY");
						tempSelectedWorldRot[1] = GUI.TextField (new Rect(70,2,50,20), tempSelectedWorldRot[1], OnScreenText.customStyles[4]);
						GUI.SetNextControlName("SelectedWorldRotZ");
						tempSelectedWorldRot[2] = GUI.TextField (new Rect(123,2,50,20), tempSelectedWorldRot[2], OnScreenText.customStyles[5]);
						GUI.EndGroup();
					}
					if(GUI.Button(new Rect(174,72,20,20), "X", OnScreenText.customStyles[9])){ //If we pressed the "scale" button
						cont.gizmo.SelectedTransform.localScale = new Vector3(1f,1f,1f);
						updateTransformTextfields(3);
						cont.history.addHistory();
					}
					GUI.BeginGroup(new Rect(0,70,175,24), OnScreenText.customStyles[6]); //ScaleTextboxGroup
					GUI.Label (new Rect(5,6,10,20),"S");
					GUI.SetNextControlName("SelectedScaX");
					tempSelectedSca[0] = GUI.TextField (new Rect(17,2,50,20), tempSelectedSca[0], OnScreenText.customStyles[3]);
					GUI.SetNextControlName("SelectedScaY");
					tempSelectedSca[1] = GUI.TextField (new Rect(70,2,50,20), tempSelectedSca[1], OnScreenText.customStyles[4]);
					GUI.SetNextControlName("SelectedScaZ");
					tempSelectedSca[2] = GUI.TextField (new Rect(123,2,50,20), tempSelectedSca[2], OnScreenText.customStyles[5]);
					GUI.EndGroup();
					GUI.SetNextControlName("SelectedButtons");
					transformTextFieldInt = GUI.Toolbar(new Rect(5,93,165,20), transformTextFieldInt, new String[]{"Local", "World"}, OnScreenText.customStyles[8]); //Changes from local to world coordinates
				}				else{					blockInformationText = new Rect(80, 0, 200, 20);				}
				cont.config.togglePrecisionMenu = GUI.Toggle(new Rect(5,0,125,20), cont.config.togglePrecisionMenu, "Precision menu", GUI.skin.customStyles[11]);
				GUI.EndGroup();
			}						/*****************************			Materials Tab			******************************/			GUI.skin = OnScreenText;			GUI.BeginGroup(materialsTabRect);			if(cont.config.toggleMaterialMenu){ //If the precision edit window is visible				materialsTabRect = new Rect(Screen.width-200, 0, 200, 400);				GUI.BeginGroup(new Rect(0,20,200,380), OnScreenText.customStyles[6]);								if(GUI.Button(new Rect(5,5,80,20), "GET", OnScreenText.customStyles[16])){
					if(cont.gizmo.SelectedTransform){						DiffuseColor = cont.gizmo.SelectedTransform.renderer.material.color;						EmissiveColor = cont.gizmo.SelectedTransform.renderer.material.GetColor("_Emission");
						pixelTex = cont.gizmo.SelectedTransform.renderer.material.mainTexture;
						PixelIdentification temp = cont.gizmo.SelectedTransform.GetComponent<PixelIdentification>();
						pixelTexture = temp.mat;
					}				}				if(GUI.Button(new Rect(115,5,80,20), "SET", OnScreenText.customStyles[16])){
					if(cont.gizmo.SelectedTransform){						cont.gizmo.SelectedTransform.renderer.material.color = DiffuseColor;						cont.gizmo.SelectedTransform.renderer.material.SetColor("_Emission", EmissiveColor);						cont.blockfactory.setTexture(cont.gizmo.SelectedTransform.gameObject, pixelTexture);
					}
				}								//Diffuse				GUI.BeginGroup(new Rect(0,25,200,105));				GUI.Label (new Rect(5,5f,100,20),"Diffuse");				GUI.Label (new Rect(5,25,20,20),"R");				GUI.Label (new Rect(5,45,20,20),"G");				GUI.Label (new Rect(5,65,20,20),"B");				GUI.Label (new Rect(5,85,20,20),"A");				DiffuseColor.r = GUI.HorizontalSlider(new Rect(25, 25, 165, 15), DiffuseColor.r, 0.0F, 1.0F, OnScreenText.customStyles[17], OnScreenText.horizontalSliderThumb);				DiffuseColor.g = GUI.HorizontalSlider(new Rect(25, 45, 165, 15), DiffuseColor.g, 0.0F, 1.0F, OnScreenText.customStyles[18], OnScreenText.horizontalSliderThumb);				DiffuseColor.b = GUI.HorizontalSlider(new Rect(25, 65, 165, 15), DiffuseColor.b, 0.0F, 1.0F, OnScreenText.customStyles[19], OnScreenText.horizontalSliderThumb);				DiffuseColor.a = GUI.HorizontalSlider(new Rect(25, 85, 165, 15), DiffuseColor.a, 0.0F, 1.0F);				GUI.color = DiffuseColor;				GUI.Label(new Rect(75, 5, 120, 15), "", OnScreenText.customStyles[14]);				GUI.color = new Color(1f,1f,1f,1f);				GUI.EndGroup();								//Emissive				GUI.BeginGroup(new Rect(0,130,200,105));				GUI.Label (new Rect(5,5,100,20),"Emissive");				GUI.Label (new Rect(5,25,20,20),"R");				GUI.Label (new Rect(5,45,20,20),"G");				GUI.Label (new Rect(5,65,20,20),"B");				EmissiveColor.r = GUI.HorizontalSlider(new Rect(25, 25, 165, 15), EmissiveColor.r, 0.0F, 1.0F, OnScreenText.customStyles[17], OnScreenText.horizontalSliderThumb);				EmissiveColor.g = GUI.HorizontalSlider(new Rect(25, 45, 165, 15), EmissiveColor.g, 0.0F, 1.0F, OnScreenText.customStyles[18], OnScreenText.horizontalSliderThumb);				EmissiveColor.b = GUI.HorizontalSlider(new Rect(25, 65, 165, 15), EmissiveColor.b, 0.0F, 1.0F, OnScreenText.customStyles[19], OnScreenText.horizontalSliderThumb);				GUI.color = EmissiveColor;				GUI.Label(new Rect(75, 5, 120, 15), "", OnScreenText.customStyles[14]);				GUI.color = new Color(1f,1f,1f,1f);				GUI.EndGroup();				
				
				materialsTexInt = GUI.Toolbar(new Rect(2,215,196,20), materialsTexInt, new String[]{"N8", "Particle", "Symbol"}, OnScreenText.customStyles[15]);
				materialScrollPosition = GUI.BeginScrollView(new Rect(0, 235, 200, 145), materialScrollPosition, new Rect(0, 0, 180, currentNumTextures*20));
					for(int i = 0; i < currentNumTextures; i++){ 
						Texture2D tex = (Texture2D)materialTextures[i];
						if(GUI.Button(new Rect(0,i*20,180,20), tex.name)){
							pixelTex = tex;
							pixelTexture = resourcePrefix + tex.name + ".dds";
						}
					}
				GUI.EndScrollView();
								GUI.EndGroup();			}			else{				materialsTabRect = new Rect(Screen.width-200, 0, 200, 20);			}			cont.config.toggleMaterialMenu = GUI.Toggle(new Rect(5,0,125,20), cont.config.toggleMaterialMenu, "Materials", GUI.skin.customStyles[11]);			GUI.EndGroup();									/*****************************
			Start pixel placing buttons			******************************/
			/*GUI.skin = OnScreenText;
			GUILayout.BeginArea(pixelButtonsArea); //Define the area to place the pixel creation buttons
			GUILayout.BeginVertical();
			if(GUILayout.Button("",GUI.skin.customStyles[0])){
				cont.input.beginPlacingVoxel(cont.blockfactory.createPixel(0)); //Place a tpixel
			}
			GUILayout.FlexibleSpace();
			if(GUILayout.Button("",GUI.skin.customStyles[1])){
				cont.input.beginPlacingVoxel(cont.blockfactory.createPixel(1)); //Place a pixel2
			}
			GUILayout.EndVertical();
			GUILayout.EndArea();*/						/*****************************			Debug History information			******************************/
			int temphistory = (cont.history.getHistoryCount() == 0) ? 0 : cont.history.getHistoryLocation()+1; //Special check if the count of the history list is 0, otherwise it would say 1/0
			GUI.Label(new Rect(125,Screen.height-30,300,30),"History: " + temphistory + " / " + cont.history.getHistoryCount());
			GUI.Label(new Rect(125,Screen.height-15,200,20),"[F2]<---->[F3]");						/*****************************			Window Handlers			******************************/
			GUI.skin = Windows;
			if(showOpenFileDialog){
				openFileDialog = GUI.Window(0, openFileDialog, windowDialog, "Open a File");
			}
			if(showLoginRegister){
				loginRegisterDialog = GUI.Window(2, loginRegisterDialog, windowDialog, (loginRegisterInt == 0) ?  "Login" : "Register");
			}
			if(showPopupDialog){
				popupDialog = GUI.Window(100, popupDialog, windowDialog, "Hey!");
			}
		}		
		if(cont.showFPS){ //If we are currently showing the FPS at the top right.
			GUI.skin = OnScreenText;
			if(cont.fps >= 55)
				GUI.skin.customStyles[2].normal.textColor = Color.green;
			else if(cont.fps >= 30)
				GUI.skin.customStyles[2].normal.textColor = Color.yellow;
			else
				GUI.skin.customStyles[2].normal.textColor = Color.red;

			GUI.Label(new Rect(Screen.width-105,5,100,10), cont.formattedFPS, GUI.skin.customStyles[2]);
		}
		
			//This controls the tooltip that is shown when you mouse over certain objects. The math was a pain in the ass
			int xmodify, ymodify;
			Vector2 tool = EscapeMenu.GetStyle("ToolTip").CalcSize(new GUIContent(GUI.tooltip));
			
			if(Input.mousePosition.x>= Screen.width-(int)tool.x-10){ xmodify = -(int)tool.x-10; EscapeMenu.customStyles[3].alignment = TextAnchor.UpperRight;}
			else{ xmodify = 20; EscapeMenu.customStyles[3].alignment = TextAnchor.UpperLeft;}
			if(Screen.height-Input.mousePosition.y >= Screen.height-10) ymodify = 0;
			else ymodify = 0;
			if(GUI.tooltip != ""){
				GUI.Label(new Rect(Input.mousePosition.x+xmodify, Screen.height-Input.mousePosition.y+ymodify, Mathf.Min(300f,tool.x),20), GUI.tooltip,  EscapeMenu.customStyles[3]);
			}
			
			//This controls the notification the show up on the screen, and gracefully fades them in and out based on how old they are, and finally delete them when they are too old.
			if(notificationsScreen != null && notificationsTime != null && notificationsScreen.Count != 0 && notificationsScreen.Count ==  notificationsTime.Count){
					float alphaModifier;
					for(int i = 0; i < notificationsScreen.Count;i++){
						if((float)notificationsTime[i]+1f >Time.realtimeSinceStartup){ //Start fading in if <1 second old
							alphaModifier = (Time.realtimeSinceStartup - (float)notificationsTime[i]);
						}
						else if((float)notificationsTime[i]+5f < Time.realtimeSinceStartup){ //Start fading out if >5 seconds old
							alphaModifier = 1-(Time.realtimeSinceStartup - (float)notificationsTime[i]-5f);
						}
						else{ alphaModifier = 1f; }
						if((float)notificationsTime[i]+6f < Time.realtimeSinceStartup){ //If they reached the end of their life, delete
							notificationsScreen.RemoveAt(i);
							notificationsTime.RemoveAt(i);
							i--;
						}
						else{
							GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alphaModifier);
							Vector2 nottemp = EscapeMenu.customStyles[15].CalcSize(new GUIContent((string)notificationsScreen[i])); //Dynamically gets/sets the size of the notification so it looks nicer
							GUI.Label(new Rect(Screen.width-nottemp.x-4, Screen.height-(24*(i+1)), nottemp.x+4, 24), (string)notificationsScreen[i], EscapeMenu.customStyles[15]);
						}
					}
					GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, 1f);
			}
	}
	
	//Controls windows that popup on the screen
	void windowDialog(int windowID){
		GUI.skin = Windows;
		if(windowID == 0){ //block loading
			blockURL = GUILayout.TextField(blockURL, 300, GUILayout.Width(300));
			if(GUILayout.Button("Load")){
				StartCoroutine(cont.blockfactory.loadN8BlockURL(blockURL));
				showOpenFileDialog = false;
				subWindow();
			}
			GUI.DragWindow();
		}
		
		if(windowID == 100){ //popup
			GUI.BringWindowToFront(100);
			GUILayout.Label(popupString);
		}
	}
	
	//Create a popup window
	public void popup(string str){
		popupString = str;
		if(!showPopupDialog){
			showPopupDialog = true;
		}
		addWindow();
	}
	
	//Close the popup window
	public void closePopup(){
		showPopupDialog = false;
		popupString = "";
		subWindow();
	}
	
	//Called when adding a window to the screen
	public void addWindow(){
		numWindowsOpen++;
		Time.timeScale = 0f;
	}
	
	//Called when closing a window on the screen
	public void subWindow(){
		if(numWindowsOpen > 0) numWindowsOpen--;
		if(numWindowsOpen == 0) Time.timeScale = 1f;
	}
	
	//This function is called when we want to send a notification to the user, and store it in the database in the ESC menu
	public void notification(string str){
		DateTime temp = DateTime.Now;
		if(notificationsScreen == null){
			notificationsScreen = new ArrayList();
			notificationsTime = new ArrayList();
		}
		notificationsScreen.Insert(0, str);
		notifications.Add("[" + temp.Hour.ToString("00")  + ":" + temp.Minute.ToString("00") + ":" + temp.Second.ToString("00")  + "] " + str);
		notificationsTime.Insert(0, Time.realtimeSinceStartup);
		doNotificationScrollBottom = true;
	}
	
	//Use if you want to update all of the fields of the precision edit menu at once
	public void updateTransformTextfields(){
		updateTransformTextfields(1);
		updateTransformTextfields(2);
		updateTransformTextfields(3);
	}
	
	//Useful if we only need to update specific fields of the precision edit menu
	public void updateTransformTextfields(int i){
		if(cont.gizmo.SelectedTransform){
			if(i == 1){ //Position
				cont.gui.tempSelectedPos[0] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.localPosition.x);
				cont.gui.tempSelectedPos[1] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.localPosition.y);
				cont.gui.tempSelectedPos[2] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.localPosition.z);
				cont.gui.tempSelectedWorldPos[0] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.position.x);
				cont.gui.tempSelectedWorldPos[1] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.position.y);
				cont.gui.tempSelectedWorldPos[2] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.position.z);
			}
			else if(i == 2){ //Rotation
				cont.gui.tempSelectedRot[0] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.localRotation.eulerAngles.x);
				cont.gui.tempSelectedRot[1] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.localRotation.eulerAngles.y);
				cont.gui.tempSelectedRot[2] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.localRotation.eulerAngles.z);
				cont.gui.tempSelectedWorldRot[0] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.rotation.eulerAngles.x);
				cont.gui.tempSelectedWorldRot[1] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.rotation.eulerAngles.y);
				cont.gui.tempSelectedWorldRot[2] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.parent.rotation.eulerAngles.z);
			}
			else if(i == 3){ //Scale
				cont.gui.tempSelectedSca[0] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.localScale.x);
				cont.gui.tempSelectedSca[1] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.localScale.y);
				cont.gui.tempSelectedSca[2] = String.Format("{0:0.###}", cont.gizmo.SelectedTransform.localScale.z);
			}
		}
	}		private IEnumerator updatePixelColors(){		tpixelthing.renderer.material.color = DiffuseColor;		tpixelthing.renderer.material.SetColor("_Emission", EmissiveColor);
		tpixelthing.renderer.material.mainTexture = pixelTex;		pixel2thing.renderer.material.color = DiffuseColor;		pixel2thing.renderer.material.SetColor("_Emission", EmissiveColor);
		pixel2thing.renderer.material.mainTexture = pixelTex;		yield return null;	}
}
