using UnityEngine;
using System.Collections;

public class BlockHistory : MonoBehaviour {
	//TODO: Make it work on a "change" based system. Like addPixel(pos,rot,color,blah) or delPixel(name), so that it doesn't lag as much with large objects
	
	private Control cont; //Reference to control object that controls everything
	private ArrayList history = new ArrayList(50);
	private int historyLocation;

	void Start () {
		cont = GameObject.Find("Control").GetComponent<Control>();
		clearHistory(); //set the history to a default state
	}
	
	public void addHistory(){
		string temp = cont.blockfactory.saveOutBlock();
		if(history.Count >= cont.config.historySize)
			history.RemoveAt(0);
		
		if(historyLocation < history.Count-1)
			for(int i = history.Count-1; i > historyLocation; i--)
				history.RemoveAt(i);
				
		history.Add(temp);
		historyLocation = history.Count-1;
	}
	
	public void clearHistory(){
		string temp = "";
		if(history.Count > 0) temp = history[historyLocation].ToString();
		history = new ArrayList(50);
		history.Add(temp);
		historyLocation = 0;
	}
	
	public int getHistoryCount(){
		return history.Count;
	}
	
	public int getHistoryLocation(){
		return historyLocation;
	}
	
	public void undo(){
		if(historyLocation > 0){
			historyLocation--;
			cont.blockfactory.loadN8Block(history[historyLocation].ToString());
		}else{
			historyLocation = 0;
			cont.blockfactory.loadN8Block(history[historyLocation].ToString());
		}
	}
	
	public void redo(){
		if(historyLocation < history.Count-1){
			historyLocation++;
			cont.blockfactory.loadN8Block(history[historyLocation].ToString());//cont.blockfactory.loadN8Block();
		}else{
			if(history.Count != 0) historyLocation = history.Count-1;
			else historyLocation = 0; 
			cont.blockfactory.loadN8Block(history[historyLocation].ToString());
		}
	}
}
