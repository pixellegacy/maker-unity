using UnityEngine;
using System.Collections;

public class HexGridSnap : MonoBehaviour {
  
 static float sizehex,h,r,b,a;
 
 static void Init (float sizetemp) {
  HexGridSnap.sizehex = sizetemp;
  h = Mathf.Sin(30*Mathf.Deg2Rad) * sizehex;
  r = Mathf.Cos(30*Mathf.Deg2Rad) * sizehex;
  b = sizehex + 2 * h;
  a = 2 * r;
 }
 
 static Vector3 Snap(Vector3 p) {
  var u = p.x;
  var v = p.z;
  var x = u - u % a + r;
  var y = v - v % b + ((x - r) / a % 1) * b / 2;
  return new Vector3(x, p.y, y);
 }

}