using UnityEngine;
using System.Collections;
using System.IO;

public class EncodeToPNG : MonoBehaviour {
	private Rect saveCameraRect;
	private Control cont;
	
	void Start(){
		cont = GameObject.Find("Control").GetComponent<Control>();
		saveCameraRect = new Rect(Screen.width-32,0,32,32);
		GameObject.Find("SaveCamera").camera.pixelRect = saveCameraRect;
	}
	
	IEnumerator TakePNG () {
		GameObject sky = GameObject.Find("SkyBox").gameObject;
		GameObject grid = GameObject.Find("Grid").gameObject;
		sky.active = false;
		grid.active = false;
		// Create a texture the size of the screen, RGB24 format
		Texture2D tex = new Texture2D (32, 32, TextureFormat.ARGB32, false);
		// Read screen contents into the texture
		tex.ReadPixels (saveCameraRect, 0, 0);
		tex.Apply ();
		//tex.Resize (width-1, height-1, TextureFormat.ARGB32, false);
		
		sky.active=true;
		grid.active=true;
		
		// Encode texture into PNG
		byte[] bytes = tex.EncodeToPNG();
		Destroy (tex);

		//Create a Web Form
		WWWForm form = new WWWForm();
		form.AddBinaryData("file", bytes, "test.png");
		print(form.data);
		WWW w = new WWW("http://www.pixellegacy.com/maker/upload.php", form);
		yield return w;

		// Upload to a cgi script    
		if (w.error != null) {
			cont.gui.popup(w.error);
		} else {
			cont.gui.popup("Finished Uploading Screenshot");    
		}
	}
}
