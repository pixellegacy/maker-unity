using UnityEngine;
using System.Collections;

public class ColorHelper : MonoBehaviour {

	void Start(){
	}
	
	public float[] COLORtoHSL(Color c){
		float[] hsl = new float[4];
		float rgbmax = 0f, rgbmin = 1f;
		if(c.r > rgbmax) rgbmax = c.r;
		if(c.r < rgbmin) rgbmin = c.r;
		if(c.g > rgbmax) rgbmax = c.g;
		if(c.g < rgbmin) rgbmin = c.g;
		if(c.b > rgbmax) rgbmax = c.b;
		if(c.b < rgbmin) rgbmin = c.b;
		
		hsl[2] = (rgbmax + rgbmin)/2; //L
		if(rgbmax != rgbmin){
			if(hsl[2] < 0.5f) hsl[1]=(rgbmax-rgbmin)/(rgbmax+rgbmin);
			else hsl[1]=(rgbmax-rgbmin)/(2.0f-rgbmax-rgbmin);
		}
		if(c.r == rgbmax) hsl[0] = (c.g-c.b)/(rgbmax-rgbmin);
		else if(c.g == rgbmax) hsl[0] = 2.0f +(c.b-c.r)/(rgbmax-rgbmin);
		else if(c.g == rgbmax) hsl[0] = 4.0f +(c.r-c.g)/(rgbmax-rgbmin);
		
		hsl[0] *= 60f;
		if(hsl[0] < 0) hsl[0] += 360;
		hsl[1] *= 100f;
		hsl[2] *= 100f;
		hsl[3] = c.a;
		return hsl;
	}
	
	public Color HSLtoCOLOR(float[] hsl){
		hsl[0] /= 60f;
		hsl[1] /= 100f;
		hsl[2] /= 100f;
		Color c = new Color();
		float temp2, temp1, temp3;
		if(hsl[1] == 0){
			c.r = hsl[2];
			c.g = hsl[2];
			c.b = hsl[2];
			c.a = hsl[3];
		}
		else{
			if(hsl[2] < 0.5f) temp2 = hsl[2]*(1.0f+hsl[1]);
			else temp2 = hsl[2]+hsl[1] - hsl[2]*hsl[1];
			temp1 = 2.0f*hsl[2] - temp2;
			hsl[0] /= 360f;
			
			//R
			temp3 = hsl[0]+1.0f/3.0f;
			if(temp3 < 0f) temp3 += 1.0f;
			if(temp3 > 1f) temp3 -= 1.0f;
			if(6.0f*temp3 < 1f) c.r = temp1+(temp2-temp1)*6.0f*temp3;
			else if(2.0f*temp3 < 1f) c.r = temp2;
			else if(3.0f*temp3 < 2f) c.r = temp1+(temp2-temp1)*((2.0f/3.0f)-temp3)*6.0f;
			else c.r = temp1;
			//G
			temp3 = hsl[0];
			if(temp3 < 0f) temp3 += 1.0f;
			if(temp3 > 1f) temp3 -= 1.0f;
			if(6.0f*temp3 < 1f) c.g = temp1+(temp2-temp1)*6.0f*temp3;
			else if(2.0f*temp3 < 1f) c.g = temp2;
			else if(3.0f*temp3 < 2f) c.g = temp1+(temp2-temp1)*((2.0f/3.0f)-temp3)*6.0f;
			else c.g = temp1;
			//B
			temp3 = hsl[0]-1.0f/3.0f;
			if(temp3 < 0f) temp3 += 1.0f;
			if(temp3 > 1f) temp3 -= 1.0f;
			if(6.0f*temp3 < 1f) c.b = temp1+(temp2-temp1)*6.0f*temp3;
			else if(2.0f*temp3 < 1f) c.b = temp2;
			else if(3.0f*temp3 < 2f) c.b = temp1+(temp2-temp1)*((2.0f/3.0f)-temp3)*6.0f;
			else c.b = temp1;
			c.a = hsl[3];
		}
		return c;
	}
}
