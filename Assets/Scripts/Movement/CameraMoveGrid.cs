using UnityEngine;
using System.Collections;
using System;

public class CameraMoveGrid : MonoBehaviour {
	public float distanceCap;
	private float horizontalMove;
	private float forwardMove;
	private Vector3 lastPosition;
	public Transform Gizmo;
	public Control cont;
	
	void Start () {
		cont = GameObject.Find("Control").GetComponent<Control>();
		distanceCap = 100f;
	}
		
	void Update () {
		if(!cont.input.disableMovement){
			lastPosition = transform.position;
			forwardMove= Input.GetAxis("Vertical") * Time.deltaTime * cont.config.movementSpeed;
			horizontalMove = Input.GetAxis("Horizontal") * Time.deltaTime * cont.config.movementSpeed;
			transform.Translate(horizontalMove, 0, forwardMove);
			if(Vector3.Distance(Vector3.zero, transform.position) >= distanceCap){
				transform.position = lastPosition;
			}
			float temp = Vector3.Distance(Gizmo.position, transform.position)/cont.config.gizmoSize;
			Vector3 vectemp = new Vector3(temp,temp,temp);
			if((vectemp = applySnap(vectemp,.25f)) != Gizmo.localScale && vectemp != Vector3.zero){
				Gizmo.localScale = vectemp;
			}
		}
	}

	public Vector3 applySnap(Vector3 vect, float snap){
		snap = snap*100f;
		return new Vector3((float)Math.Round(vect.x / snap, 2, MidpointRounding.ToEven) * snap, (float)Math.Round(vect.y / snap, 2, MidpointRounding.ToEven) * snap, (float)Math.Round(vect.z / snap, 2, MidpointRounding.ToEven) * snap);
	}
}
