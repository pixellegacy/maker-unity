using UnityEngine;
using System.Collections;
using System;

public class GizmoControl : MonoBehaviour {

	public Transform Gizmo;
	public Transform SelectedTransform;
	public Control cont;
	public Material redGizmo, blueGizmo, greenGizmo;
	public Vector3 dragDirection;
	public int gizmoInt;
	private int colorHighlight;
	public bool disableRay;
	
	void Start(){
		cont = GameObject.Find("Control").GetComponent<Control>();
		dragDirection = Vector3.zero;
		disableRay = false;
		highlightGizmo(0);
	}
	
	//Some weird snapping to grid function
	public Vector3 applySnap(Vector3 vect, float snap){
		snap = snap*100f;
		return new Vector3((float)Math.Round(vect.x / snap, 2, MidpointRounding.ToEven) * snap, (float)Math.Round(vect.y / snap, 2, MidpointRounding.ToEven) * snap, (float)Math.Round(vect.z / snap, 2, MidpointRounding.ToEven) * snap);
	}
	
	//This function recursively navigates the stack until it finds a block object, or returns null. Only used in the screensaver project I made a while ago.
	public GameObject findBlockFromRay(GameObject obj){
		if(obj.CompareTag("Block")){
			return obj;
		}
		else if(obj.transform.parent == null){
			return null;
		}
		else{
			return findBlockFromRay(obj.transform.parent.gameObject);
		}
	}
	
	public void SelectObject(GameObject obj){
		if(SelectedTransform != null){ //Unselect everything if there's something selected
			UnselectObjects();
		}
		SelectedTransform = obj.transform;
		Transform[] children = SelectedTransform.parent.GetComponentsInChildren<Transform>();
		foreach(Transform child in children){
			if(!child.gameObject.GetComponent<WireframeMesh>() && obj.transform != child){ // if doesn't already have wireframe
				if(child.gameObject.tag == "Pixel" ){
					WireframeMesh tempcomp = child.gameObject.AddComponent<WireframeMesh>();
					tempcomp.lineColor = cont.config.selectionColorAlt;
				}
				/*else if(child.gameObject.name == "pixelsafecomponent"){
					child.renderer.enabled = true;
					child.renderer.material.color = cont.config.safePixelColorAlt;
				}*/
			}
			else if(obj.transform == child){
				WireframeMesh tempcomp = child.gameObject.AddComponent<WireframeMesh>();
				tempcomp.lineColor = cont.config.selectionColor;					
				//child.parent.Find("pixelsafecomponent").renderer.enabled = true;
				//child.parent.Find("pixelsafecomponent").renderer.material.color = cont.config.safePixelColor;
			}
		}
		Gizmo.parent = SelectedTransform.parent;
		Gizmo.position = SelectedTransform.parent.position;
		//Gizmo.position = SelectedTransform.collider.bounds.center;
		Gizmo.localRotation = Quaternion.identity;
		Gizmo.gameObject.SetActiveRecursively(true);
		cont.gui.updateTransformTextfields();
	}
	
	public void destroySelected(){
		if(SelectedTransform){
			Gizmo.parent = null; //Make sure we don't delete the gizmo so we unparent it
			Gizmo.gameObject.SetActiveRecursively(false); //Turn the gizmo off
			DestroyImmediate(SelectedTransform.parent.gameObject);
			SelectedTransform = null;
			highlightGizmo(0);
		}
	}
	
	public void UnselectObjects(){
		if(SelectedTransform){
			Transform[] children = SelectedTransform.parent.GetComponentsInChildren<Transform>();
			foreach(Transform child in children){
				if(child.gameObject.GetComponent<WireframeMesh>() && child.gameObject.tag == "Pixel"){ // if doesn't already have wireframe
					DestroyImmediate(child.gameObject.GetComponent<WireframeMesh>());
				}
				if(child.gameObject.name == "pixelsafecomponent")
					child.renderer.enabled = false;
			}
			SelectedTransform = null;
		}
		highlightGizmo(0);
		Gizmo.parent = null;
		Gizmo.gameObject.SetActiveRecursively(false);
	}
	
	public void highlightGizmo(int colorHighlight){
		gizmoInt = colorHighlight;
		highlightGizmo();
	}
	
	public void adjustPosition(Vector3 vect){
		if(SelectedTransform) SelectedTransform.parent.Translate(Vector3.Scale(new Vector3(cont.config.positionSnap, cont.config.positionSnap, cont.config.positionSnap), vect));
		cont.gui.updateTransformTextfields(1);
	}
	public void adjustRotation(Quaternion quat){
		if(SelectedTransform) SelectedTransform.parent.Rotate(Vector3.Scale(new Vector3(cont.config.rotationSnap, cont.config.rotationSnap, cont.config.rotationSnap), quat.eulerAngles));
		cont.gui.updateTransformTextfields(2);
	}
	public void adjustScale(Vector3 scale){
		if(SelectedTransform) SelectedTransform.localScale += Vector3.Scale(new Vector3(cont.config.scaleSnap, cont.config.scaleSnap, cont.config.scaleSnap), scale);
		cont.gui.updateTransformTextfields(3);
	}
	
	private void highlightGizmo(){
		switch(gizmoInt){
			case(1):
				redGizmo.SetColor("_Color",new Color(255f,0f,0f,.8f));
				blueGizmo.SetColor("_Color",new Color(0f,0f,255f,.5f));
				greenGizmo.SetColor("_Color",new Color(0f,255f,0f,.5f));
			break;
			case(2):
				redGizmo.SetColor("_Color",new Color(255f,0f,0f,.5f));
				blueGizmo.SetColor("_Color",new Color(0f,0f,255f,.5f));
				greenGizmo.SetColor("_Color",new Color(0f,255f,0f,.8f));
			break;
			case(3):
				redGizmo.SetColor("_Color",new Color(255f,0f,0f,.5f));
				blueGizmo.SetColor("_Color",new Color(0f,0f,255f,.8f));
				greenGizmo.SetColor("_Color",new Color(0f,255f,0f,.5f));
			break;
			case(0):
				redGizmo.SetColor("_Color",new Color(255f,0f,0f,.5f));
				blueGizmo.SetColor("_Color",new Color(0f,0f,255f,.5f));
				greenGizmo.SetColor("_Color",new Color(0f,255f,0f,.5f));
			break;
		}
	}
	
	public virtual IEnumerator ScaleObject()
	{
		if(SelectedTransform){
			float initialMouse = Input.mousePosition.x;
			Vector3 initialScale = SelectedTransform.localScale;
			Transform trans = SelectedTransform;
			while (Input.GetMouseButton (0))
			{
				float placeholder = -(initialMouse-Input.mousePosition.x)/Screen.width;
				Vector3 desiredScale = applySnap(Vector3.Scale(new Vector3(placeholder, placeholder,placeholder)*cont.config.scaleSpeed, dragDirection), cont.config.scaleSnap) + initialScale;
					if(desiredScale == Vector3.zero) trans.localScale = new Vector3(.01f,.01f,.01f);
					else trans.localScale = desiredScale;
					//Gizmo.position = SelectedTransform.collider.bounds.center;
				cont.gui.updateTransformTextfields(3);
				yield return null;
			}
			cont.history.addHistory();
		}
	}
	
	public virtual IEnumerator RotateObject()
	{
		if(SelectedTransform){
			float initialMouse = Input.mousePosition.x;
			Transform trans = SelectedTransform.parent;

			while( Input.GetMouseButton(0) )
			{
				float placeholder = (initialMouse-Input.mousePosition.x)/Screen.width;
				Vector3 desiredRot = applySnap(Vector3.Scale(new Vector3(placeholder, placeholder,placeholder)*cont.config.rotationSpeed*5, dragDirection),cont.config.rotationSnap);
				if(desiredRot != Vector3.zero){trans.Rotate(desiredRot); initialMouse = Input.mousePosition.x;}
				cont.gui.updateTransformTextfields(2);
				yield return null;
			}// end while
			cont.history.addHistory();
		}
	}
	
	public virtual IEnumerator DragObject()
	{
		if(SelectedTransform){
			float initialMouse = Input.mousePosition.x;
			Transform trans = SelectedTransform.parent;

			while( Input.GetMouseButton(0) )
			{
				float placeholder = -(initialMouse-Input.mousePosition.x)/Screen.width;
				Vector3 temp = applySnap(Vector3.Scale(new Vector3(placeholder,placeholder,placeholder)*cont.config.dragSpeed*5, dragDirection),cont.config.positionSnap);
					if(temp != Vector3.zero){ trans.Translate(temp); initialMouse = Input.mousePosition.x;}
				cont.gui.updateTransformTextfields(1);
				yield return null;
			}// end while
			cont.history.addHistory();
		}
	}
}