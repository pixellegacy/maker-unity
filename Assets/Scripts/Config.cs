using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System;

public class Config : MonoBehaviour {
	private Control cont;
	public Color selectionColor, selectionColorAlt, safePixelColor, safePixelColorAlt;
	public float movementSpeed, gizmoSize, rotationSnap, scaleSnap, positionSnap, dragSpeed, rotationSpeed, scaleSpeed;
	public int historySize;
	public KeyCode keyConfigMenu, keyMenuOk, keySaveBlock;
	public KeyCode keyCopyPixel, keyCutPixel, keyPastePixel, keyPickupPixel, keyScalePixel, keyDeletePixel, keyRotateUp, keyRotateDown, keyDragUp, keyDragDown, keyScaleUp, keyScaleDown, keyGizmoRed, keyGizmoGreen, keyGizmoBlue;
	public string userHash, authorName, userPass;
	public int autoLogin;
	public bool togglePrecisionMenu, toggleMaterialMenu;
	
	void Awake () {
		loadConfig();
	}
	
	void Start(){
		cont = GameObject.Find("Control").GetComponent<Control>();
		if(autoLogin == 1) StartCoroutine(login(authorName,userPass,1));
	}
	
	void loadConfig(){
		authorName = 			PlayerPrefs.GetString("AuthorName","Unknown");
		userPass = 				PlayerPrefs.GetString("userPass","empty");
		autoLogin = 			PlayerPrefs.GetInt("autoLogin",0);
		movementSpeed =			PlayerPrefs.GetFloat("movementSpeed",20f);
		gizmoSize =				PlayerPrefs.GetFloat("gizmoSize",7f);
		rotationSnap = 			PlayerPrefs.GetFloat("rotationSnap",15f);
		scaleSnap = 			PlayerPrefs.GetFloat("scaleSnap",.5f);
		positionSnap = 			PlayerPrefs.GetFloat("positionSnap",.5f);
		dragSpeed = 			PlayerPrefs.GetFloat("dragSpeed",10f);
		rotationSpeed = 		PlayerPrefs.GetFloat("rotationSpeed",30f);
		scaleSpeed = 			PlayerPrefs.GetFloat("scaleSpeed",10f);
		historySize = 			PlayerPrefs.GetInt("historySize",50);
		togglePrecisionMenu = 	(PlayerPrefs.GetInt("togglePrecisionMenu",0) == 1) ? true: false;
		toggleMaterialMenu = 	(PlayerPrefs.GetInt("toggleMaterialMenu",0) == 1) ? true: false;
		keyPickupPixel = 		(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keySelectPixel","LeftControl"));
		keyCopyPixel = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keySelectPixel","C"));
		keyCutPixel = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keySelectPixel","X"));
		keyPastePixel =			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keySelectPixel","V"));
		keyScalePixel = 		(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyScalePixel","LeftShift"));
		keyDeletePixel =		(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyDeletePixel","Delete"));
		keyRotateUp = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyRotateUp","Keypad6"));
		keyRotateDown =			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyRotateDown","Keypad4"));
		keyDragUp = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyDragUp","Keypad8"));
		keyDragDown = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyDragDown","Keypad2"));
		keyScaleUp = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyScaleUp","Keypad9"));
		keyScaleDown = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyScaleDown","Keypad3"));
		/*keyRotateUp = 		(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyRotateUp","Alpha6"));
		keyRotateDown =			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyRotateDown","Alpha7"));
		keyDragUp = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyDragUp","Alpha4"));
		keyDragDown = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyDragDown","Alpha5"));
		keyScaleUp = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyScaleUp","Alpha8"));
		keyScaleDown = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyScaleDown","Alpha9"));*/
		keyGizmoRed = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyGizmoRed","Alpha1"));
		keyGizmoGreen = 		(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyGizmoGreen","Alpha2"));
		keyGizmoBlue = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyGizmoBlue","Alpha3"));
		keyConfigMenu = 		(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyConfigMenu","Escape"));
		keyMenuOk = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keyMenuOk","Return"));
		keySaveBlock = 			(KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("keySaveBlock","F1"));
		selectionColor = 		new Color(PlayerPrefs.GetFloat("selColorRed",1f),PlayerPrefs.GetFloat("selColorGreen",1f),PlayerPrefs.GetFloat("selColorBlue",1f),PlayerPrefs.GetFloat("selColorAlpha",1f));
		selectionColorAlt = 	new Color(PlayerPrefs.GetFloat("selColorRedAlt",151f/255f),PlayerPrefs.GetFloat("selColorGreenAlt",170f/255f),PlayerPrefs.GetFloat("selColorBlueAlt",200f/255f),PlayerPrefs.GetFloat("selColorAlphaAlt",1f));
		safePixelColor = 		new Color(PlayerPrefs.GetFloat("safeColorRed",1f),PlayerPrefs.GetFloat("safeColorGreen",0f),PlayerPrefs.GetFloat("safeColorBlue",1f),PlayerPrefs.GetFloat("safeColorAlpha",.85f));
		safePixelColorAlt = 	new Color(PlayerPrefs.GetFloat("safeColorRedAlt",0f),PlayerPrefs.GetFloat("safeColorGreenAlt",1f),PlayerPrefs.GetFloat("safeColorBlueAlt",1f),PlayerPrefs.GetFloat("safeColorAlphaAlt",.85f));
	}
	
	public IEnumerator saveConfig(){
		try
		{
			PlayerPrefs.SetString("AuthorName",authorName);
			PlayerPrefs.SetString("userPass",userPass);
			PlayerPrefs.SetInt("autoLogin",autoLogin);
			PlayerPrefs.SetFloat("movementSpeed",movementSpeed);
			PlayerPrefs.SetFloat("gizmoSize",gizmoSize);
			PlayerPrefs.SetFloat("rotationSnap",rotationSnap);
			PlayerPrefs.SetFloat("scaleSnap",scaleSnap);
			PlayerPrefs.SetFloat("positionSnap",positionSnap);
			PlayerPrefs.SetFloat("dragSpeed",dragSpeed);
			PlayerPrefs.SetFloat("rotationSpeed",rotationSpeed);
			PlayerPrefs.SetFloat("scaleSpeed",scaleSpeed);
			PlayerPrefs.SetInt("historySize", historySize);
			PlayerPrefs.SetFloat("selColorRed",selectionColor.r);
			PlayerPrefs.SetFloat("selColorGreen",selectionColor.g);
			PlayerPrefs.SetFloat("selColorBlue",selectionColor.b);
			PlayerPrefs.SetFloat("selColorAlpha",selectionColor.a);
			PlayerPrefs.SetFloat("selColorRedAlt",selectionColorAlt.r);
			PlayerPrefs.SetFloat("selColorGreenAlt",selectionColorAlt.g);
			PlayerPrefs.SetFloat("selColorBlueAlt",selectionColorAlt.b);
			PlayerPrefs.SetFloat("selColorAlphaAlt",selectionColorAlt.a);
			PlayerPrefs.SetFloat("safeColorRed",safePixelColor.r);
			PlayerPrefs.SetFloat("safeColorGreen",safePixelColor.g);
			PlayerPrefs.SetFloat("safeColorBlue",safePixelColor.b);
			PlayerPrefs.SetFloat("safeColorAlpha",safePixelColor.a);
			PlayerPrefs.SetFloat("safeColorRedAlt",safePixelColor.r);
			PlayerPrefs.SetFloat("safeColorGreenAlt",safePixelColor.g);
			PlayerPrefs.SetFloat("safeColorBlueAlt",safePixelColor.b);
			PlayerPrefs.SetFloat("safeColorAlphaAlt",safePixelColor.a);
			PlayerPrefs.SetString("keyPickupPixel",keyPickupPixel+"");
			PlayerPrefs.SetString("keyCopyPixel",keyCopyPixel+"");
			PlayerPrefs.SetString("keyCutPixel",keyCutPixel+"");
			PlayerPrefs.SetString("keyPastePixel",keyPastePixel+"");
			PlayerPrefs.SetString("keyScalePixel",keyScalePixel+"");
			PlayerPrefs.SetString("keyDeletePixel",keyDeletePixel+"");
			PlayerPrefs.SetString("keyRotateUp",keyRotateUp+"");
			PlayerPrefs.SetString("keyRotateDown",keyRotateDown+"");
			PlayerPrefs.SetString("keyDragUp",keyDragUp+"");
			PlayerPrefs.SetString("keyDragDown",keyDragDown+"");
			PlayerPrefs.SetString("keyScaleUp",keyScaleUp+"");
			PlayerPrefs.SetString("keyScaleDown",keyScaleDown+"");
			PlayerPrefs.SetString("keyGizmoRed",keyGizmoRed+"");
			PlayerPrefs.SetString("keyGizmoBlue",keyGizmoBlue+"");
			PlayerPrefs.SetString("keyGizmoGreen",keyGizmoGreen+"");
			PlayerPrefs.SetString("keyConfigMenu",keyConfigMenu+"");
			PlayerPrefs.SetString("keyMenuOk",keyMenuOk+"");
			PlayerPrefs.SetString("keySaveBlock",keySaveBlock+"");
			PlayerPrefs.SetInt("togglePrecisionMenu",(togglePrecisionMenu)?1:0);
			PlayerPrefs.SetInt("toggleMaterialMenu",(toggleMaterialMenu)?1:0);
			cont.gui.notification("Config saved!");
		}
		catch (PlayerPrefsException e)
		{
			cont.gui.notification("Config failed to save! " + e);
		}
		yield return null;
	}
	
	public void resetConfig(){
		PlayerPrefs.DeleteAll();
		loadConfig();
		saveConfig();
		cont.gui.notification("Config successfully reset!");
	}

	//called when logging in via a gui form
	public void manualLogin(string username, string password, int remember){
		StartCoroutine(login(username,encryptString(password),remember));
	}
	
	//This assumes the password has already been encrypted elsewhere, called directly by the "autologin on start" script
	private IEnumerator login(string username, string password, int remember){
		WWWForm form = new WWWForm();
		form.AddField("username", username, System.Text.Encoding.UTF8);
		form.AddField("password", password, System.Text.Encoding.UTF8);
		WWW w = new WWW("http://www.pixellegacy.com/maker/login.php", form);
		yield return w;

		if (w.error != null) {
			cont.gui.popup(w.error);
		} else {
			if(w.text.Trim() != "failure"){
				authorName = username;
				if(remember == 1){
					userPass = password;
					autoLogin = 1;
				}
				else{
					userPass = "";
					autoLogin = 0;
				}
				string[]temp = w.text.Split(':');
				if(temp.Length > 1){ 
					userHash = temp[1].Trim();
					cont.gui.notification("Successfully logged in");
				}
				else{
					cont.gui.notification("Something went wrong. Possibly user doesn't exist.");
				}
				saveConfig();
			}
			else{
				cont.gui.notification("User doesn't exist, perhaps try to register?");
			}
		}
	}
	
	public void manualRegister(string username, string password, string email){
		StartCoroutine(register(username,encryptString(password),email));
	}
	
	public IEnumerator register(string username, string password, string email){
		WWWForm form = new WWWForm();
		form.AddField("username", username, System.Text.Encoding.UTF8);
		form.AddField("password", password, System.Text.Encoding.UTF8);
		form.AddField("email", email, System.Text.Encoding.UTF8);
		WWW w = new WWW("http://www.pixellegacy.com/maker/register.php", form);
		yield return w;

		if (w.error != null) {
			cont.gui.notification(w.error);
		} else {
			if(w.text == "user created") cont.gui.notification("User successfully created!");
			else cont.gui.notification(w.text);
		}
	}
	
	//called in order to MD5 something
	public string encryptString(string input){
		MD5 md5Hash = MD5.Create();
        byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
        StringBuilder sBuilder = new StringBuilder();
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }
        // Return the hexadecimal string.
        return sBuilder.ToString();
	}
	
	public bool verifyHash(){
		StartCoroutine(verifyAction());
		return userHash != "" ? true : false;
	}
	
	private IEnumerator verifyAction(){
		WWWForm form = new WWWForm();
		form.AddField("username", authorName, System.Text.Encoding.UTF8);
		form.AddField("hash", userHash, System.Text.Encoding.UTF8);
		WWW w = new WWW("http://www.pixellegacy.com/maker/verifyaction.php", form);
		yield return w;
		if(w.text != "true"){
			userHash = "";
			cont.gui.notification("Failed to authenticate. Possibly logged in at other location.");
		}
	}
}
