using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {
	public float XSpeed = 0f;
	public float YSpeed = 5.0f;
	public float ZSpeed = 0f;
	public bool wobble = false;
	public float XAmount = 15f;
	public float YAmount = 15f;
	public float ZAmount = 15f;
	private Quaternion rot;
	private Quaternion temp;
	
	void Start(){
		rot = transform.rotation;
	}

	// Update is called once per frame
	void Update () {
	if(!wobble){
		if(XSpeed != 0f){
			transform.Rotate(Vector3.left * Time.deltaTime * XSpeed, Space.World);
		}
		if(YSpeed != 0f){
			transform.Rotate(Vector3.up * Time.deltaTime * YSpeed, Space.World);
		}
		if(ZSpeed != 0f){
			transform.Rotate(Vector3.forward* Time.deltaTime * ZSpeed, Space.World);
		}
	}
	else{
		Quaternion tempor = rot;
		if(XAmount != 0f && XSpeed != 0f){
			temp.eulerAngles = new Vector3(Mathf.Sin(Time.time * XSpeed) * XAmount, 0f, 0f);
			tempor *= temp;
		}
		if(YAmount != 0f && YSpeed != 0f){
			temp.eulerAngles = new Vector3(0f, Mathf.Sin(Time.time * YSpeed) * YAmount, 0f);
			tempor *= temp;
		}
		if(ZAmount != 0f && ZSpeed != 0f){
			temp.eulerAngles = new Vector3(0f, 0f, Mathf.Sin(Time.time * ZSpeed) * ZAmount);
			tempor *= temp;
		}
		transform.rotation = tempor;
	}
	}
}
