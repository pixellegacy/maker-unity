using UnityEngine;
using System.Collections;

public class ClickForPhysics : MonoBehaviour {
	public GUIText GUITextToUpdate;
	
    void Update() {
		if(Screen.lockCursor){
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 50))
			if (Input.GetMouseButtonDown(0)){
				GameObject ObjRay = findBlockFromRay(hit.collider.gameObject);
				GUITextToUpdate.text = "Collided with " + ObjRay.name;
				if(ObjRay.layer == 8 && ObjRay){ //if it's a block
					if(!ObjRay.rigidbody){//if doesn't already have physics
					ObjRay.AddComponent("Rigidbody");
					GUITextToUpdate.text += " and added physics.";
					}
					else{
						Destroy(ObjRay.rigidbody);
						GUITextToUpdate.text += " and removed physics.";
					}
				}
			}
		}
    }
	
	//This function recursively navigates the stack until it finds a block object, or returns null
	public GameObject findBlockFromRay(GameObject obj){
		if(obj.CompareTag("Block") || obj == null){
			return obj;
		}
		else{
			return findBlockFromRay(obj.transform.parent.gameObject);
		}
	}
}