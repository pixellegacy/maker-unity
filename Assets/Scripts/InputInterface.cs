using UnityEngine;
using System.Collections;

public class InputInterface : MonoBehaviour
{
	private Control cont;
	public Transform placingVoxel;
	private Transform placingVoxelSafe;
	public LayerMask voxelPlaceLayers; //We should only be able to place a "voxel" on specific objects
	private int gizmoLayerMask; //Used in a raycast to check that we are clicking on the gizmo
	public Texture[] minimalCursor; //Support for a custom cursor later on
	public bool disableMovement; //If we have a window currently up, we don't want our WASD to trigger some movement
	private Vector3 rotationOffset;
	public Camera tpixelcam, pixel2cam;
	
	void Start ()
	{
		cont = GameObject.Find("Control").GetComponent<Control>();
		gizmoLayerMask = 1 << 9;
		rotationOffset = Vector3.zero;
	}
	
	void Update () 
	{
		Vector2 mouseScreen = new Vector2(Input.mousePosition.x,Screen.height-Input.mousePosition.y);
		if (cont.gui.blockInformationText.Contains(mouseScreen) || cont.gui.openFileButton.Contains(mouseScreen) || cont.gui.pixelButtonsArea.Contains(mouseScreen)||cont.gui.materialsTabRect.Contains(mouseScreen)||pixel2cam.rect.Contains(mouseScreen)||tpixelcam.rect.Contains(mouseScreen))
		{
			cont.gizmo.disableRay = true;
		}
		else
		{
			cont.gizmo.disableRay = false;
		}
		
		if( !cont.gui.showOptionsDialog )
		{
			//This following section of code sets the information about the block using the precision movement box at the top of the screen
			if(	GUI.GetNameOfFocusedControl () == "SelectedPosX" || GUI.GetNameOfFocusedControl () == "SelectedPosY" || GUI.GetNameOfFocusedControl () == "SelectedPosZ" || GUI.GetNameOfFocusedControl () == "SelectedWorldPosX" || GUI.GetNameOfFocusedControl () == "SelectedWorldPosY" || GUI.GetNameOfFocusedControl () == "SelectedWorldPosZ"|| 
				GUI.GetNameOfFocusedControl () == "SelectedRotX" || GUI.GetNameOfFocusedControl () == "SelectedRotY" || GUI.GetNameOfFocusedControl () == "SelectedRotZ" || GUI.GetNameOfFocusedControl () == "SelectedWorldRotX" || GUI.GetNameOfFocusedControl () == "SelectedWorldRotY" || GUI.GetNameOfFocusedControl () == "SelectedWorldRotZ" ||
				GUI.GetNameOfFocusedControl () == "SelectedScaX" || GUI.GetNameOfFocusedControl () == "SelectedScaY" || GUI.GetNameOfFocusedControl () == "SelectedScaZ")
			{
				//disableMovement = true;
				if(Input.GetKeyDown(KeyCode.Return))
				{
					Transform t = cont.gizmo.SelectedTransform;
					float floattemp = 0f;
					Vector3 vecttemp;
					if(float.TryParse(cont.gui.tempSelectedPos[0], out floattemp) && (vecttemp = new Vector3(floattemp, t.parent.localPosition.y, t.parent.localPosition.z)) != t.parent.localPosition && cont.gui.transformTextFieldInt == 0) t.parent.localPosition = vecttemp;
					else if(float.TryParse(cont.gui.tempSelectedWorldPos[0], out floattemp)) t.parent.position = new Vector3(floattemp, t.parent.position.y, t.parent.position.z);
					
					if(float.TryParse(cont.gui.tempSelectedPos[1], out floattemp) && (vecttemp = new Vector3(t.parent.localPosition.x, floattemp, t.parent.localPosition.z)) != t.parent.localPosition && cont.gui.transformTextFieldInt == 0) t.parent.localPosition = vecttemp;
					else if(float.TryParse(cont.gui.tempSelectedWorldPos[1], out floattemp)) t.parent.position = new Vector3(t.parent.position.x, floattemp, t.parent.position.z);
					
					if(float.TryParse(cont.gui.tempSelectedPos[2], out floattemp) && (vecttemp = new Vector3(t.parent.localPosition.x, t.parent.localPosition.y, floattemp)) != t.parent.localPosition && cont.gui.transformTextFieldInt == 0) t.parent.localPosition = vecttemp;
					else if(float.TryParse(cont.gui.tempSelectedWorldPos[2], out floattemp)) t.parent.position = new Vector3(t.parent.position.x, t.parent.position.y, floattemp);
					
					if(float.TryParse(cont.gui.tempSelectedRot[0], out floattemp) && (vecttemp = new Vector3(floattemp, t.parent.localRotation.eulerAngles.y, t.parent.localRotation.eulerAngles.z)) != t.parent.localRotation.eulerAngles && cont.gui.transformTextFieldInt == 0) t.parent.localRotation = Quaternion.Euler(vecttemp);
					else if(float.TryParse(cont.gui.tempSelectedWorldRot[0], out floattemp)) t.parent.rotation = Quaternion.Euler(floattemp, t.parent.rotation.eulerAngles.y, t.parent.rotation.eulerAngles.z);
					
					if(float.TryParse(cont.gui.tempSelectedRot[1], out floattemp) && (vecttemp = new Vector3(t.parent.localRotation.eulerAngles.x, floattemp, t.parent.localRotation.eulerAngles.z)) != t.parent.localRotation.eulerAngles && cont.gui.transformTextFieldInt == 0) t.parent.localRotation = Quaternion.Euler(vecttemp);
					else if(float.TryParse(cont.gui.tempSelectedWorldRot[1], out floattemp)) t.parent.rotation = Quaternion.Euler(t.parent.rotation.eulerAngles.x, floattemp, t.parent.rotation.eulerAngles.z);
					
					if(float.TryParse(cont.gui.tempSelectedRot[2], out floattemp) && (vecttemp = new Vector3(t.parent.localRotation.eulerAngles.x, t.parent.localRotation.eulerAngles.y, floattemp)) != t.parent.localRotation.eulerAngles && cont.gui.transformTextFieldInt == 0) t.parent.localRotation = Quaternion.Euler(vecttemp);
					else if(float.TryParse(cont.gui.tempSelectedWorldRot[2], out floattemp)) t.parent.rotation = Quaternion.Euler(t.parent.rotation.eulerAngles.x, t.parent.rotation.eulerAngles.y, floattemp);
					
					if(float.TryParse(cont.gui.tempSelectedSca[0], out floattemp)) t.localScale = new Vector3(floattemp, t.localScale.y, t.localScale.z);
					if(float.TryParse(cont.gui.tempSelectedSca[1], out floattemp)) t.localScale = new Vector3(t.localScale.x, floattemp, t.localScale.z);
					if(float.TryParse(cont.gui.tempSelectedSca[2], out floattemp)) t.localScale = new Vector3(t.localScale.x, t.localScale.y, floattemp);
					cont.gui.updateTransformTextfields();
					cont.history.addHistory();
				}
			}
			else
			{
				disableMovement = false;
			}
		
			//If we are currently placing a block
			if(placingVoxel)
			{
				//First, check to see if our safe component has been set for convenience
				if(!placingVoxelSafe)
				{ 
					placingVoxelSafe = placingVoxel.parent; 
					placingVoxelSafe.parent = null;
				}
				
				 //Find where our mouse currently is
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				float temp = Input.GetAxisRaw("Mouse ScrollWheel");
				if(temp > 0) temp = 1;
				if(temp < 0) temp = -1;
				rotationOffset += new Vector3(0,cont.config.rotationSnap*temp,0);
				
				//If it hit an area it can place the pixel on
				if(Physics.Raycast(ray, out hit, 300f, voxelPlaceLayers))
				{ 
					placingVoxelSafe.localPosition = cont.gizmo.applySnap(hit.point, cont.config.positionSnap);
					placingVoxelSafe.rotation = Quaternion.FromToRotation(placingVoxelSafe.up, hit.normal) * placingVoxelSafe.rotation  * Quaternion.Euler(cont.gizmo.applySnap(rotationOffset, cont.config.rotationSnap));
					rotationOffset = Vector3.zero;
					
					if(Input.GetKeyDown(KeyCode.LeftShift))
					{
						cont.gizmo.UnselectObjects();
						Destroy(placingVoxelSafe.gameObject);
						placingVoxel = null;
						placingVoxelSafe = null;
					}
					if(Input.GetMouseButtonDown(0))
					{
						if(hit.collider.gameObject.tag == "Pixel")
						{
							placingVoxelSafe.parent =  hit.collider.gameObject.transform.parent;
						}
						endPlacingVoxel();
					}
				}
			}
			else
			{
				if (Input.GetMouseButtonDown(0) && tpixelcam.pixelRect.Contains(Input.mousePosition))
					cont.input.beginPlacingVoxel(cont.blockfactory.createPixel(0, cont.gui.pixelTexture, cont.gui.DiffuseColor, cont.gui.EmissiveColor,"False")); //Place a tpixel
				else if (Input.GetMouseButtonDown(0) && pixel2cam.pixelRect.Contains(Input.mousePosition))
					cont.input.beginPlacingVoxel(cont.blockfactory.createPixel(1, cont.gui.pixelTexture, cont.gui.DiffuseColor, cont.gui.EmissiveColor,"False")); //Place a pixel2
				
				if(Time.timeScale != 0f && !cont.gizmo.disableRay)
				{	
					//Start left click on something
					if (Input.GetMouseButtonDown(0) && !cont.input.placingVoxel)
					{ 
						Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //Find where clicked
						RaycastHit hit;
						
						//If it hit the gizmo
						if(Physics.Raycast(ray, out hit, Mathf.Infinity, gizmoLayerMask))
						{ 
							GameObject ObjRay = hit.collider.gameObject;
							if(ObjRay.name == "RedPos" || ObjRay.name == "RedNeg")
							{
								cont.gizmo.gizmoInt = 1;
								cont.gizmo.dragDirection = -Vector3.left;
								if(Input.GetKey(cont.config.keyScalePixel))
									StartCoroutine(cont.gizmo.ScaleObject());
								else
									StartCoroutine(cont.gizmo.DragObject());
							}
							else if(ObjRay.name == "BluePos" || ObjRay.name == "BlueNeg")
							{
								cont.gizmo.gizmoInt = 3;
								cont.gizmo.dragDirection = -Vector3.forward;
								if(Input.GetKey(cont.config.keyScalePixel))
									StartCoroutine(cont.gizmo.ScaleObject());
								else
									StartCoroutine(cont.gizmo.DragObject());
							}
							else if(ObjRay.name == "GreenPos" || ObjRay.name == "GreenNeg")
							{
								cont.gizmo.gizmoInt = 2;
								cont.gizmo.dragDirection = Vector3.up;
								if(Input.GetKey(cont.config.keyScalePixel))
									StartCoroutine(cont.gizmo.ScaleObject());
								else
									StartCoroutine(cont.gizmo.DragObject());
							}
							else if(ObjRay.name == "RedRotate")
							{
								cont.gizmo.gizmoInt = 1;
								cont.gizmo.dragDirection = -Vector3.forward;
								StartCoroutine(cont.gizmo.RotateObject());
							}
							else if(ObjRay.name == "BlueRotate")
							{
								cont.gizmo.gizmoInt = 3;
								cont.gizmo.dragDirection = -Vector3.left;
								StartCoroutine(cont.gizmo.RotateObject());
							}
							else if(ObjRay.name == "GreenRotate")
							{
								cont.gizmo.gizmoInt = 2;
								cont.gizmo.dragDirection = -Vector3.up;
								StartCoroutine(cont.gizmo.RotateObject());
							}
							cont.gizmo.highlightGizmo(cont.gizmo.gizmoInt);
						}
						
						//If it hit anything else
						else if(Physics.Raycast(ray, out hit))
						{ 
							GameObject ObjRay = hit.collider.gameObject;
							if(ObjRay.CompareTag("Pixel"))
							{
								if(Input.GetKey(cont.config.keyScalePixel) && ObjRay.transform == cont.gizmo.SelectedTransform)
								{
									cont.gizmo.dragDirection = new Vector3(1f,1f,1f);
									StartCoroutine(cont.gizmo.ScaleObject());
								}
								else if(Input.GetKey(cont.config.keyPickupPixel) && Input.GetKey(cont.config.keyCopyPixel))
								{
										beginPlacingVoxel(cont.blockfactory.duplicatePixel(ObjRay));
								}
								else if(Input.GetKey(cont.config.keyPickupPixel) && !Input.GetKey(cont.config.keyCopyPixel))
								{
										beginPlacingVoxel(ObjRay);
								}
								else
								{
									cont.gizmo.SelectObject(ObjRay);
								}
							}
							else{
								cont.gizmo.UnselectObjects();
							}
						}
						
						//Didn't hit something
						else
						{
							cont.gizmo.UnselectObjects();
						}
					}
				}
				
				if(Time.timeScale != 0f)
				{
					if(Input.GetKeyDown(cont.config.keyDeletePixel))
					{
						cont.gizmo.destroySelected();
						cont.history.addHistory();
					}
					else if(Input.GetKeyDown(cont.config.keyGizmoRed))
					{
						cont.gizmo.highlightGizmo(1);
					}
					else if(Input.GetKeyDown(cont.config.keyGizmoGreen))
					{
						cont.gizmo.highlightGizmo(2);
					}
					else if(Input.GetKeyDown(cont.config.keyGizmoBlue))
					{
						cont.gizmo.highlightGizmo(3);
					}
					else if(Input.GetKeyDown(cont.config.keyDragUp))
					{
						int m = cont.gizmo.gizmoInt;
						if(m == 1) cont.gizmo.adjustPosition(-Vector3.left);
						if(m == 2) cont.gizmo.adjustPosition(Vector3.up);
						if(m == 3) cont.gizmo.adjustPosition(Vector3.forward);
					}
					else if(Input.GetKeyDown(cont.config.keyDragDown))
					{
						int m = cont.gizmo.gizmoInt;
						if(m == 1) cont.gizmo.adjustPosition(Vector3.left);
						if(m == 2) cont.gizmo.adjustPosition(-Vector3.up);
						if(m == 3) cont.gizmo.adjustPosition(-Vector3.forward);
					}
					else if(Input.GetKeyDown(cont.config.keyRotateUp))
					{
						int m = cont.gizmo.gizmoInt;
						if(m == 1) cont.gizmo.adjustRotation(Quaternion.Euler(Vector3.forward));
						if(m == 2) cont.gizmo.adjustRotation(Quaternion.Euler(Vector3.up));
						if(m == 3) cont.gizmo.adjustRotation(Quaternion.Euler(-Vector3.left));
					}
					else if(Input.GetKeyDown(cont.config.keyRotateDown))
					{
						int m = cont.gizmo.gizmoInt;
						if(m == 1) cont.gizmo.adjustRotation(Quaternion.Euler(-Vector3.forward));
						if(m == 2) cont.gizmo.adjustRotation(Quaternion.Euler(-Vector3.up));
						if(m == 3) cont.gizmo.adjustRotation(Quaternion.Euler(Vector3.left));
					}
					else if(Input.GetKeyDown(cont.config.keyScaleUp))
					{
						int m = cont.gizmo.gizmoInt;
						if(m == 1) cont.gizmo.adjustScale(-Vector3.left);
						if(m == 2) cont.gizmo.adjustScale(Vector3.up);
						if(m == 3) cont.gizmo.adjustScale(Vector3.forward);
					}
					else if(Input.GetKeyDown(cont.config.keyScaleDown))
					{
						int m = cont.gizmo.gizmoInt;
						if(m == 1) cont.gizmo.adjustScale(Vector3.left);
						if(m == 2) cont.gizmo.adjustScale(-Vector3.up);
						if(m == 3) cont.gizmo.adjustScale(-Vector3.forward);
					}
					/*else if(Input.GetKeyDown(cont.config.keySaveBlock)){
						StartCoroutine(cont.blockfactory.takeThumbnail());
					}*/
					else if(Input.GetKeyDown(KeyCode.F2))
					{
						cont.history.undo();
					}
					else if(Input.GetKeyDown(KeyCode.F3))
					{
						cont.history.redo();
					}
					else if(Input.GetKeyDown(KeyCode.F4))
					{
						cont.history.clearHistory();
					}
				}
			}
		}
		if(Input.GetKeyDown(cont.config.keyConfigMenu))
		{
			if(cont.gui.showOptionsDialog)
			{ 
				cont.gui.showOptionsDialog = false;
				cont.gui.subWindow();
			}
			else
			{
				cont.gui.showOptionsDialog = true;
				cont.gui.addWindow();
			}
		}
	}
	
	public void beginPlacingVoxel(GameObject obj)
	{
		obj.layer = 12;
		setAllChildrenLayers(obj.transform, 12);
		cont.gizmo.UnselectObjects();
		cont.input.placingVoxel = obj.transform;
	}
	
	public void endPlacingVoxel()
	{
		cont.gizmo.UnselectObjects();
		placingVoxel.gameObject.layer = 0;
		setAllChildrenLayers(placingVoxel, 0);
		placingVoxel = null;
		placingVoxelSafe = null;
		cont.history.addHistory();
	}
	
	public void setAllChildrenLayers(Transform obj, int i)
	{
		if(obj.parent)
		{
			Transform[] temp = obj.parent.gameObject.GetComponentsInChildren<Transform>();
			foreach(Transform child in temp) if(child != obj) if(child.gameObject.tag=="Pixel") child.gameObject.layer = i;
		}
	}
}