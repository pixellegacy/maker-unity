Shader "N8Shader" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Emission ("Emission", Color) = (0,0,0,1)
		_Highlight ("Highlight", Color) = (0,.9,1,0)
	}

	SubShader {
		Tags {"RenderType"="Opaque"}
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		fixed4 _Color;
		fixed4 _Emission;
		fixed4 _Highlight;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Emission = _Emission.rgb;
			if(_Highlight.a > 0){
			o.Emission.rgb = 0f;
			o.Albedo = (tex2D(_MainTex, IN.uv_MainTex) * _Highlight).rgb;
			}
		}
		ENDCG
	}

	Fallback "Diffuse"
}
