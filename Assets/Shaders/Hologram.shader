Shader "Hologram" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Pass {
			Tags { "Queue" = "Transparent" "RenderType"="Transparent" }
			Fog { Mode Off }
			//ZWrite off
			//ZTest Less
			Blend SrcAlpha OneMinusDstAlpha
			AlphaTest GEqual 0.1
			LOD 200
			
			CGPROGRAM
// Upgrade NOTE: excluded shader from DX11 and Xbox360; has structs without semantics (struct v2f members alpha)
#pragma exclude_renderers d3d11 xbox360
// Upgrade NOTE: excluded shader from Xbox360; has structs without semantics (struct v2f members alpha)
#pragma exclude_renderers xbox360
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _MainTex;

			struct v2f {
				float4 pos : SV_POSITION;
				float alpha;
				float2 uv : TEXCOORD0;
			};
			
			v2f vert (appdata_base v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				float3 C = WorldSpaceViewDir(v.vertex);
				float3 N = mul(_World2Object, float4(v.normal, 0)).xyz;
				float NdotC = dot(N, C);
				//Clamp it??
				NdotC = clamp(NdotC, 0, 1);
				o.uv.x = NdotC;
				o.uv.y = 0;
				//Use col for alpha
				o.alpha = 1-NdotC;
				return o;
			}

			half4 frag( v2f i ) : COLOR {
				half4 c = tex2D (_MainTex, i.uv);
				c.a = i.alpha;
				return c;
			}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}