using UnityEngine;
using System.Collections;
using System.IO;

public class LockCursor : MonoBehaviour {
	public GUITexture cursor;
	private Vector3 originalPosition = new Vector3(.5f,.5f,0);
	private Vector3 outsidePosition = new Vector3(0,100,0);
	public Rect cursorPosAndSize = new Rect(0,0,16,16);
	
	private void Awake(){
		Screen.lockCursor = true;
	}
	
	IEnumerator Start() {
        WWW www = new WWW("file://" + Path.GetDirectoryName(Application.dataPath) + "/cursor.png");
        yield return www;
        cursor.texture = www.texture;
	}
	
    private void Update() {
		if (Input.GetKeyDown(KeyCode.F1)){
			if(Screen.lockCursor){ //if currently locked
				cursor.transform.position = outsidePosition;
			}else{
				cursor.transform.position = originalPosition;
			}
            Screen.lockCursor = !Screen.lockCursor;
		}
    }
}